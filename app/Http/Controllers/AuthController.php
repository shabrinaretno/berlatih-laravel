<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index(){
        return view('halaman.regis');
    }
    public function welcome(Request $request){
        $first = $request['FN'];
        $last = $request['LN'];
        return view('halaman.welcome', compact('first', 'last'));
        //dd($request->all());
    }
}
